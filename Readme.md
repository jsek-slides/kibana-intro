# Kibana - The missing introduction

Live on [kibana-intro.surge.sh](http://kibana-intro.surge.sh)

## Setup

```shell
npm i
```

### Run on localhost

```shell
npm run watch
```

and in another terminal tab

```shell
npm run serve
```
