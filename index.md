---
marp: true
title: Kibana - Introduction
description: The missing introduction of Kibana capabilities
theme: uncover
paginate: true
_paginate: false
---

<style>
@import url('https://fonts.googleapis.com/css?family=Oxygen|PT+Mono');
section {
  font-family: 'Oxygen', 'Ubuntu', 'Segoe UI', sans-serif;
  letter-spacing: 1px;
}
li > code,
p > code {
  font-family: 'PT Mono', 'Hasklig', 'Ubuntu Mono', Consolas, monospace;
  font-size: .9em;
  background-color: transparent;
  color: #c7a;
}
pre {
    padding: 1.4em 2em;
}
.hljs-commen t {
    color: #888 !important;
    font-style: italic;
    font-size: .8em;
}

table {
  width: 80%;
}
td, th {
  padding: 2% 3% !important;
}
td:first-child,
th:first-child {
  border-right: 3px solid #666;
}
</style>

![h:300](images/google/logo.png)

The missing introduction

---

![bg w:85%](images/jokes/elk-1.png)

---

![bg h:85%](images/survey.png)

---

## Agenda

- Kibana vs Grafana vs DataDog
- Logging from .NET Core
- Filtering logs
- Creating dashboards

---

[![bg h:95%](images/google/trends.png)](https://trends.google.com/trends/explore?geo=US&q=kibana,Grafana,Datadog)

---

![bg w:95%](images/google/big-picture.png)

---

![bg h:85%](images/jokes/nsa.jpg)

---

![bg w:85%](images/google/observability.png)

![bg w:95%](images/google/pyramid.png)

---

![bg w:95%](images/google/zipkin.png)

---

![bg w:95%](images/google/trace-id.png)

---

![bg w:95%](images/google/grafana.png)

---

|         | Kibana | Grafana | DataDog    |
| ------- | ------ | ------- | ---------- |
| Logs    | ★      | Loki    | since 2018 |
| Metrics | Beats  | ★       | ★          |
| Traces  | v6+    | Kausal  | since 2017 |
| | | | |
| $$$     | High   | Low     | depends    |

---

![bg h:85%](images/jokes/datacat.png)

---

[![bg h:100%](images/google/datadog.png)](https://www.sec.gov/Archives/edgar/data/1561550/000119312519227783/d745413ds1.htm)

---

![bg w:100%](images/jokes/elk-0.png)

![bg w:90%](images/google/elk.png)

---

## Seq?

- Logs
- Quick visualization
- Up and running < 60s

![bg left w:100%](images/google/seq.png)

---

![bg right h:45%](images/google/dotnet.png)

## Serilog [[1]](https://medium.com/fabridamazio/asp-net-core-logging-with-serilog-eb7c5fdea234)

- structured logging
- easy configuration
- most popular?

---

![bg h:95%](images/google/trends-2.png)

---

![bg h:95%](images/google/fields.png)

---

```sh
> dotnet add package Serilog
> dotnet add package Serilog.Sinks.Console
```

```cs
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();
```

```cs
Log.Information("Hello {name}!", "John");
```

---

```cs
var config = new LoggerConfiguration()
    .MinimumLevel.Information()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    .MinimumLevel.Override("System", LogEventLevel.Warning)
    .WriteTo.Console();
```

---

```cs
if (env.IsDevelopment())
    config = config.WriteTo.Seq("http://localhost:5341");

if (canStoreLogs)
    config = config.WriteTo.File(
        $@"{AzureLogsDir}\{env.ApplicationName}.txt",
        fileSizeLimitBytes: 5_000_000,
        rollOnFileSizeLimit: true,
        shared: true,
        flushToDiskInterval: TimeSpan.FromSeconds(1)
    );
```

---

```cs
if (!env.IsDevelopment())
{
    var uri = new Uri("http://localhost:9200");
    var options = new ElasticsearchSinkOptions(uri){
        IndexFormat = "demo-{0:yyyy.MM.dd}",
        AutoRegisterTemplate = true
    };

    config = config.WriteTo.Elasticsearch(options);
}
```

---

```cs
Log.Logger = config.CreateLogger();
```

```cs
AppDomain.CurrentDomain.ProcessExit += (s, e) => Log.CloseAndFlush();
```

```cs
services.AddSingleton(Log.Logger);
```

---

```cs
var template = "HTTP {RequestMethod} {RequestPath}"
             + " responded {StatusCode} in {Elapsed:0.0} s";

logger.Information(template,
    httpContext.Request.Method,
    httpContext.Request.Path,
    statusCode,
    (float)sw.ElapsedMilliseconds / 1000
);
```

---

![bg w:85%](images/chat-1.png)

---

## Demo 1

- filter events [[v5.5]](https://www.elastic.co/guide/en/kibana/5.5/field-filter.html)
- auto-reload
- save query

---

![bg h:100%](images/google/filters.png)

---

## Demo 2

1. visualize
2. design dashboard

---

![bg h:100%](images/google/dashboard.png)

---

## Hands-on demo

[Download project with guidelines](demo.7z)

---

## Outline

- Step 1 - create new project
- Step 2 - add Serilog
- Step 3 - configuration for Elasticsearch
- Step 4 - run with Docker
- Step 5 - upgrade to v7.4.0

---

- Step 6 (bonus)
  - Watch [Effective Logging in ASP.NET Core | Pluralsight](http://bit.ly/2IVZujN)
  - Implement attribute-based logging
  - Visualize request timings in Kibana
